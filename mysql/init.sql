-- Создание новой базы данных с именем parsedb, если она еще не существует
CREATE DATABASE IF NOT EXISTS parsedb;

-- Выбор базы данных parsedb для последующих операций
USE parsedb;

-- Создание таблицы links, если она еще не существует
CREATE TABLE IF NOT EXISTS links (
    id INT AUTO_INCREMENT PRIMARY KEY,  -- Колонка id, автоматически увеличивающаяся, используемая как первичный ключ
    url VARCHAR(255) NOT NULL           -- Колонка url для хранения ссылок, тип данных VARCHAR с максимальной длиной 255 символов, не может быть NULL
);
