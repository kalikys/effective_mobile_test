# Web Scraper Project

Данный проект представляет собой тестовое задание для собеседования в Effective Mobile.
## Основные функции 🚀

- **Ввод URL**: Пользователи могут ввести любой URL для извлечения данных.
- **Парсинг страниц**: Автоматическое извлечение всех ссылок с веб-страниц.
- **Сохранение данных**: Результаты парсинга сохраняются в MySQL базу данных.
- **Просмотр результатов**: Пользователи могут просматривать сохраненные данные через веб-интерфейс.

## Технологии 🛠

- **Frontend**: HTML, CSS (Bootstrap)
- **Backend**: PHP
- **Database**: MySQL
- **Infrastructure**: Docker, Docker Compose, Ansible

## Начало работы 🌟

### Предварительные требования

Для работы приложения убедитесь, что установлены:

- [Docker](https://docs.docker.com/get-docker/)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Установка и запуск

### Запуск на локальной машине

```bash
git clone https://gitlab.com/kalikys/effective_mobile_test.git
cd effective_mobile_test
docker compose up
```

#### Доступ к приложению

После запуска откройте веб-приложение в браузере:

```
http://localhost:8080

ИЛИ

http://localhost:8080/index.php

http://localhost:8080/display.php
```

### Запуск на удаленной машине с помощью Ansible
<span style="color: white;">**❗Внимание:** Запуск ansible-playbook производится из</span><span style="color: red;"> корня проекта</span>

#### Настройка файла ansible/hosts.yml

Укажите ip адрес машины и путь к ssh ключу

Для удобства использования можно создать папку ansible/source_files

В нее помещаются 3 файла: 
- **SSH ключ**: ssh_key
- **Токен для авторизации в gitlab registry**: gitlab_token
- **Ip адреса машин**: ips.yml

```yml
#ips.yml

main_ip: "yourip"
```

```yml
#side.yml

vars_files:
- ansible/source_files/ips.yml
```

Далее в файле ansible/side.yml выберите интересующие Вас роли.

После всех манипуляций можно запускать развертывание:

```bash
ansible-playbook -i ansible/hosts.yml ansible/side.yml 
```

#### Доступ к приложению

После запуска откройте веб-приложение в браузере:

```
http://yourip:8080

ИЛИ

http://yourip:8080/index.php

http://yourip:8080/display.php
```

## Как использовать? 📘

Просто введите URL в поле на главной странице и нажмите "Парсить". После обработки URL вы можете просмотреть результаты, кликнув на ссылку для просмотра сохраненных данных.

## Как внести вклад? 👥

Как предложить улучшение:

1. Fork (ответвление) репозитория.
2. Создайте свою Feature Branch (`git checkout -b feature/AmazingFeature`).
3. Сделайте Commit ваших изменений (`git commit -m 'Add some AmazingFeature'`).
4. Push в Branch (`git push origin feature/AmazingFeature`).
5. Откройте Pull Request.

