<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Сохраненные ссылки</title>
    <!-- Подключение Bootstrap для стилизации страницы -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            padding: 20px; /* Установка внутренних отступов для тела страницы */
        }
        .container {
            margin-top: 20px; /* Верхний отступ для контейнера с содержимым */
        }
    </style>
</head>
<body>
<div class="container">
    <!-- Заголовок страницы -->
    <h1 class="mb-4">Список сохраненных ссылок</h1>
    <?php
    // Установка параметров подключения к базе данных
    $host = getenv("MYSQL_HOST");
    $user = getenv("MYSQL_USER");
    $pass = getenv("MYSQL_PASSWORD");
    $db = getenv("MYSQL_DB");
    
    // Создание подключения к базе данных
    $conn = new mysqli($host, $user, $pass, $db);

    // Проверка наличия ошибок подключения
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // SQL запрос для выборки всех сохраненных ссылок
    $sql = "SELECT url FROM links";
    $result = $conn->query($sql);

    // Проверка наличия результатов и их вывод
    if ($result->num_rows > 0) {
        // Начало списка для отображения ссылок
        echo '<ul class="list-group">';
        // Перебор каждой строки результата
        while($row = $result->fetch_assoc()) {
            // Вывод URL, используя функцию htmlspecialchars для предотвращения XSS атак
            echo '<li class="list-group-item">' . htmlspecialchars($row['url']) . '</li>';
        }
        // Закрытие списка
        echo '</ul>';
    } else {
        // Сообщение, если ссылок нет
        echo "<p>Нет сохраненных данных.</p>";
    }
    // Закрытие подключения к базе данных
    $conn->close();
    ?>
</div>
</body>
</html>
