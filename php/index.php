<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Парсинг сайта</title>
    <!-- Подключение Bootstrap CSS для стилизации страницы -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            padding: 20px; /* Отступы вокруг содержимого тела */
        }
        .container {
            margin-top: 20px; /* Верхний отступ для контейнера */
        }
    </style>
</head>
<body>
<div class="container">
    <!-- Заголовок формы -->
    <h1 class="mb-4">Введите адрес сайта для парсинга ссылок v1</h1>
    <!-- Форма для ввода URL и выбора действия -->
    <form method="POST" action="">
        <div class="mb-3">
            <label for="url" class="form-label">URL</label>
            <!-- Поле ввода для URL -->
            <input type="url" class="form-control" id="url" name="url">
        </div>
        <!-- Кнопки для отправки формы для парсинга или очистки базы данных -->
        <button type="submit" name="action" value="parse" class="btn btn-primary">Парсить</button>
        <button type="submit" name="action" value="clear" class="btn btn-danger">Очистить базу данных</button>
    </form>
    <?php
    // Подключение к базе данных MySQL
    $host = getenv("MYSQL_HOST");
    $user = getenv("MYSQL_USER");
    $pass = getenv("MYSQL_PASSWORD");
    $db = getenv("MYSQL_DB");

    $conn = new mysqli($host, $user, $pass, $db);
    
     // Обработка ошибок подключения
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Обработка POST запросов
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        switch ($_POST['action']) {
            case 'parse':
                if (!empty($_POST['url'])) {
                    $url = filter_var($_POST['url'], FILTER_SANITIZE_URL);
                    $context = stream_context_create(["http" => ["header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64)"]]);
                    $html = file_get_contents($url, false, $context);
                    
                    // Обработка успешного получения HTML
                    if ($html !== false) {
                        $doc = new DOMDocument();
                        @$doc->loadHTML($html);
                        $anchors = $doc->getElementsByTagName('a');
                        
                        foreach ($anchors as $element) {
                            $link = $conn->real_escape_string($element->getAttribute('href'));
                            $sql = "INSERT INTO links (url) VALUES ('$link')";
                            $conn->query($sql);
                        }
                        echo "<div class='alert alert-success mt-4'>Data successfully parsed and saved to the database. <a href='display.php' class='alert-link'>View saved links</a>.</div>";
                    } else {
                        echo "<div class='alert alert-danger mt-4'>Failed to fetch the URL: $url</div>";
                    }
                } else {
                    echo "<div class='alert alert-danger mt-4'>Please enter a URL.</div>";
                }
                break;
            
            case 'clear':
                // SQL запрос для очистки таблицы ссылок
                $sql = "TRUNKATE TABLE links";
                if ($conn->query($sql)) {
                    echo "<div class='alert alert-success mt-4'>Database successfully cleared. <a href='display.php' class='alert-link'>View saved links</a>.</div>";
                } else {
                    echo "<div class='alert alert-danger mt-4'>Error clearing database: " . $conn->error . "</div>";
                }
                break;
        }
    }

    // Закрытие соединения с базой данных
    $conn->close();
    ?>


</div>
</body>
</html>
